Software
==========

Emteq Expression Engine for Windows Application Programming Interface
------------------------------------------------------------------------

This document describes the API for the emteq Expression Engine wrapper for
Microsoft Windows.

The software consists of 3 files (2 C header files and 1 windows DLL):

|*expressionDefs.h* - a definitions (of data types) header file
|*expEngWin.h* - a functions header file
|*expEngWin.dll* - a 32 or 64-bit windows DLL (as requested)

expEngWin.h has the following function definitions::

	bool initialiseExpressionEngine(bool bTestMode); // must be called first
	bool isConnected(); // are we connected to the headset?
	bool isSettled(); // is headset settled on the face?
	int getSyncsLost(); // number of times connection has stuttered
	Float getNoiseFloor(); // noise floor should be <0.01 when sensors are seated correctly
	int getExpression(); // return the expression currently being detected
	Float getExpressionIntensity(); // return power of expression DEPRECATED use getEventIntensity
	Float getFacsIntensity(int iFacs, bool bLeft); // return the intensity of the facial action unit
	Float getEventIntensity(int iEvent); // return the intensity of the requested event
	Float getExpressivity(); // return the average amount of time spent in expressions
	int getHeartRate(); // return the heart rate in BPM (-1 if not detected)
	int getEmgChannels(); // return number of channels in emg data
	bool getEmgInputData(Float *fInputArray, int iGranularity); // return raw emg data from the headset
	Float getHeadRotation( int iMemsChannel); // return the rotation of the head in this axis
	void setMemsValue(int iMemsDevice, int iMemsChannel, // pass motion data to the engine for analysis (probably
	Float fValue); // taken from a VR accelerometer)
	Float getMemsValue(int iMemsDevice, int iMemsChannel); // return MEMS device value
	unsigned int get1khzFrameNumber(); // return the current 1khz frame count
	unsigned int get50hzFrameNumber(); // return the current 50hz frame count
	Float getEmgChannelPower(int iEmgChannel); // return the power in an EMG channel
	bool isEmgChannelSettled(int iEmgChannel); // indicates if the EMG channel sensor is settled on the face
	Float getEmgChannelContact(int iEmgChannel); // return the contact quality of an EMG channel’s sensor
	bool saveRawData(bool bEnable, int iAnalysis, int iEvent); // stream raw data from faceteq to file
	bool setAnalysisMethod(int iAnalysis, int iMethod); // set this analysis type's method
	int getAnalysisMethod(int iAnalysis); // get the method for this analysis type
	bool setIgnoreEvents(int iAnalysis, bool b); // set this analysis type to ignore any events
	bool getIgnoreEvents(int iAnalysis); // get the ignore events status for this analysis type
	bool setPause(bool b); // turn pause on/off
	bool getPause(); // is paused?
	bool getAnalysisName(int iAnalysis, char *sAnalysis); // get the name of an analysis type
	bool getEventName(int iAnalysis, int iEvent, char *sEvent); // get the name of an event
	bool getFacsName(char *sFacs, int iFacs); // get the name of a facial action unit
	bool getVersion(char *sVersion); // get the faceteq hardware version
	bool createUser(const char* sUserID); // create & set userID as the current user (copy winkR from default user)
	bool setUser(const char* sUserID); // set userID as the current user (loading any trained data for that user)
	bool getUser(char *sUser); // get the name of the currently set user

example::

	#include "windows.h" // general windows header
	#include "expEngWin.h" // main emteq header file
	int main()
	{
	bool bSuccess = expEngine::initialiseExpressionEngine(false); // startup (test mode=false)
	if (bSuccess)
	{
	char sVersion[MAX_PATH]; expEngine::getVersion(sVersion); // retrieve version number
	expEngine::setAnalysisMethod(kAnalyseExpression, kMethodSVM); // set analysis method to SVM
	expEngine::createUser("NewUser"); // create a new user
	while (true)
	{
	int iConnected = expEngine::isConnected(); // connected to hardware?
	int iSettled = expEngine::isSettled(); // sensors settled on face?
	Float fNoise = expEngine::getNoiseFloor(); // how much residual noise
	int iHeartRate = expEngine::getHeartRate(); // get BPM
	eExpression exp = expEngine::getExpression(); // get facial expression
	Float fIntense = expEngine::getExpressionIntensity(); // get intensity of expression
	Float fExpress = expEngine::getExpressivity(); // get expressivity metric
	system("cls"); // clear screen & print results
	printf("con:%d settled:%d (%.3f) hr:%d expression:%d (%.1f) expressivity:%1.2f",
	iConnected, iSettled, fNoise, iHeartRate, exp, fIntense, fExpress);
	for (int i=1; i<kExpressions; i++) // list expressions learnt state
	{
	char sExpression[MAX_PATH]; expEngine::getEventName(kAnalyseExpression, i, sExpression);
	bool b = expEngine::learntFileExists(kAnalyseExpression, i);
	printf(" %02d: %-18s: %s\n", i, sExpression, b ? "learnt" : "not learnt");
	}
	Sleep(200); // wait 200ms
	}
	}
	return 0;
	}
