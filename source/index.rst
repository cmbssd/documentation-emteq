.. Emteq Documentation documentation master file, created by
   sphinx-quickstart on Fri May 22 14:26:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Emteq Versioned Documentation with LJMU Requests
=================================================

.. toctree::
   :maxdepth: 2
   :hidden:

   about
   software
   hardware
   contents

This document is designed to be versioned with respect to hardware and software versions

`Readthedocs <https://readthedocs.org/>`_ Offer free hosting of documentation for any project and can provide pdf and epub hard copies with each version. Link a git repo to their service and create branches for each version. Master branch = Latest Docs. See the Black lower nav for version switching and downloads.

Version Latest refers to current iteration of Emteq hardware supplied to Liverpool John Moores University NSP:

* Faceteq Protoype V.4 

.. image:: ftv3.jpg

If you are unsure which device version you possess and your protoype does not have a serial number please contact `Emteq <https://emteq.net/>`_ 

:doc:`/hardware`

* Emteq Acquisiton Software version unknown

:doc:`/software`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
