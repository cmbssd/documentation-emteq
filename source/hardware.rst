Hardware
=========

Faceteq Protoype V.4
----------------------------

.. image:: ftv3.jpg

Date of receipt by LJMU NSP: Feb 2020

| Signal: 24-bit signal resolution
| fEMG Sample Rate (Hz): 1000
| PPG Sample Rate (Hz): 500
| Onboard Filtering: 20-450Hz. 
| Additional Sensors: Integrated 9DOF accelerometer-gyroscope and photo-plethysmograph (PPG) pulse rate sensors
| Supports ASCII data files as well as binary files for post- acquisition data analysis.